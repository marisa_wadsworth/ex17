﻿using System;

namespace ex17
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            //Declear Variables
            string guestAnswer;

            Console.WriteLine("Do you like puppies or kittens ?");
            Console.WriteLine();

            guestAnswer = Console.ReadLine();
            Console.WriteLine();

            //If statement
            if(guestAnswer == "puppies" || guestAnswer == "Puppies" || guestAnswer == "puppy")
            {
                Console.WriteLine("YAY ! They are my favourite too");
                Console.WriteLine();
            }

            //else
            else if(guestAnswer == "kittens" || guestAnswer == "Kittens" || guestAnswer == "kitten")
            {
                Console.WriteLine("MEWOOOOO");
            }
            
            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
